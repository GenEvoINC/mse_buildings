import scipy.interpolate as interpolate
lines = []
lines.append("1 	    86.2	  52.12     51.85	      60.20    41.12	65.92    77.03     	  84.27")
lines.append("2 		89.51	  0     0	      0    0	0    0     	  0")
lines.append("3 		90.40	  0     0	      0    0	0    0     	  0")
lines.append("4 		91.23	  0     0	      0    0	0    0     	  0")
lines.append("5 		92.01	  66.79     67.16	      77.03    60.31	87.66    90.82     	  92.43")
lines.append("6 		92.55	  0     0	      0    0	0    0     	  0")
lines.append("7 		93.26	  0     0	      0    0	0    0     	  0")
lines.append("8 		93.86	  0     0	      0    0	0    0     	  0")
lines.append("9 		94.21	  0     0	      0    0	0    0     	  0")
lines.append("10		94.45	  73.22     73.35	      82.35    70.26	92.84    92.44     	  94.55")
lines.append("11		94.69	  0     0	      0    0	0    0     	  0")
lines.append("12 		94.81	  0     0	      0    0	0    0     	  0")
lines.append("13 		95.05	  0     0	      0    0	0    0     	  0")
lines.append("14 		95.17	  0     0	      0    0	0    0     	  0")
lines.append("15 		95.47	  0     0	      0    0	0    0     	  0")
lines.append("16 		95.64	  0     0	      0    0	0    0     	  0")
lines.append("17 		95.70	  0     0	      0    0	0    0     	  0")
lines.append("18 		95.76	  0     0	      0    0	0    0     	  0")
lines.append("19 		95.88	  79.45     79.56	      88.23    80.66	94.60    92.33     	  96.22")

ls = [[]]
for line in lines:
   line = line.replace("	"," ")
   while "  " in line:
       line = line.replace("  ", " ")
   ls.append(line.split(" "))

ls.pop(0)
Xsmooth = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]
for i in range(0,18):
   x = [0,4,9,18]
   y = [ls[i][i],ls[4][i],ls[9][i],ls[18][i]]
   print(len(ls))
   for k in range(0,19):
       ls[i][k] = "%.2f" %(interpolate.pchip_interpolate(x,y,Xsmooth)[k])



for i in range(0,len(ls)):
    line  = ""
    for j in range(0,len(ls[i])):
        line = line + ls[i][j] + " "
    print(line)
